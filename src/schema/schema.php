<?php

namespace PHPGraphQLServer\Schema;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;
use PHPGraphQLServer\Schema\User\UserDao;

// MAIN QUERY
$queryType = new ObjectType([
	'name' => 'Query',
	'fields' => [
		'hello' => [
			'type' => Type::string(),
			'resolve' => function() {
				return 'Hello worldsdd!';
			}
		],
	],
]);

$schema = new Schema([
	'query'		=> $queryType,
]);
