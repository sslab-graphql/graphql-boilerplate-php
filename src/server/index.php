<?

namespace PHPGraphQLServer;

require '../vendor/autoload.php';

use GraphQL\GraphQL;
use GraphQL\Server\StandardServer;
use Overblog\DataLoader\Promise\Adapter\Webonyx\GraphQL\SyncPromiseAdapter;
use Overblog\PromiseAdapter\Adapter\WebonyxGraphQLSyncPromiseAdapter;

try {

	$graphQLPromiseAdapter = new SyncPromiseAdapter();
	$dataLoaderPromiseAdapter = new WebonyxGraphQLSyncPromiseAdapter($graphQLPromiseAdapter);

	require "../schema/schema.php";

	GraphQL::setPromiseAdapter($graphQLPromiseAdapter);

	$server_config = [
		'schema'		=> $schema,
		'context'		=> null,
		'queryBatching'	=> true,
	];

	$gql_server = new StandardServer($server_config);
	$gql_server->handleRequest();
}
catch (\Exception $e)
{
	StandardServer::send500Error($e);
}
